package com.obsidian.vampireapi.price_provider;

import lombok.Builder;
import lombok.Data;

import java.net.URL;

@Builder
@Data
public final class PriceProviderDTO {

    private String name;

    private URL url;
}
