package com.obsidian.vampireapi.price_provider;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PriceProviderMapper {

    PriceProviderDTO toDTO (PriceProvider priceProvider);

    PriceProvider fromDTO (PriceProviderDTO priceProviderDTO);
}
