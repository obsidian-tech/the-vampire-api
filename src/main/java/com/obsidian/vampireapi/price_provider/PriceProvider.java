package com.obsidian.vampireapi.price_provider;

import com.obsidian.vampireapi.core.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.net.URL;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Entity
public final class PriceProvider extends BaseEntity {

    @Id
    private final UUID id = UUID.randomUUID();

    private String name;

    private URL url;
}
