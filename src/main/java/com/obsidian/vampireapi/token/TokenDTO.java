package com.obsidian.vampireapi.token;

import com.obsidian.vampireapi.price_provider.PriceProviderDTO;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public final class TokenDTO {

    private String address;

    private String name;

    private String ticker;

    private Integer decimals;

    private Float price;

    private PriceProviderDTO priceProvider;
}
