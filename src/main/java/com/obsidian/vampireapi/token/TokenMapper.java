package com.obsidian.vampireapi.token;

import com.obsidian.vampireapi.mixed_models.TokenHasPriceProvider;
import com.obsidian.vampireapi.price_provider.PriceProvider;
import com.obsidian.vampireapi.price_provider.PriceProviderMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
@RequiredArgsConstructor
public abstract class TokenMapper {

    private final PriceProviderMapper priceProviderMapper;

    public TokenDTO toDTO(Token token) {
        return TokenDTO.builder()
                .address(token.getAddress())
                .decimals(token.getDecimals())
                .name(token.getName())
                .ticker(token.getTicker())
                .priceProvider(priceProviderMapper.toDTO(
                        token.getPriceProviders().stream()
                                .filter(TokenHasPriceProvider::getMain)
                                .map(TokenHasPriceProvider::getPriceProvider)
                                .findAny()
                                .orElse(PriceProvider.builder().name("Unknown Provider").build())
                ))
                .build();
    }
}
