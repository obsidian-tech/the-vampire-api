package com.obsidian.vampireapi.token;

import com.obsidian.vampireapi.chain.Chain;
import com.obsidian.vampireapi.core.BaseEntity;
import com.obsidian.vampireapi.mixed_models.TokenHasPriceProvider;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Entity
public final class Token extends BaseEntity {

    @Id
    private String address;

    private String name;

    private String ticker;

    private Integer decimals;

    private Float price;

    @ManyToOne
    @JoinColumn(name = "id", nullable = false)
    private Chain chain;

    @OneToMany(mappedBy = "token", fetch = FetchType.EAGER)
    private Set<TokenHasPriceProvider> priceProviders;
}
