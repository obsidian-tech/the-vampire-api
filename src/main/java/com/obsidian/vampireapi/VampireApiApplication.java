package com.obsidian.vampireapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VampireApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VampireApiApplication.class, args);
	}

}
