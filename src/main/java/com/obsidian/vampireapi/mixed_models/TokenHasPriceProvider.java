package com.obsidian.vampireapi.mixed_models;

import com.obsidian.vampireapi.price_provider.PriceProvider;
import com.obsidian.vampireapi.token.Token;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public final class TokenHasPriceProvider {
    @EmbeddedId
    private TokenHasPriceProviderId id;

    private Boolean main;

    @ManyToOne
    @MapsId("tokenId")
    @JoinColumn(name = "token_id")
    private Token token;

    @ManyToOne
    @MapsId("priceProviderId")
    @JoinColumn(name = "price_provider_id")
    private PriceProvider priceProvider;
}
