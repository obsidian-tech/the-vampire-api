package com.obsidian.vampireapi.mixed_models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.UUID;

@Embeddable
@Data
@NoArgsConstructor
public final class TokenHasPriceProviderId {

    @Column(name = "token_id")
    private String tokenId;

    @Column(name = "price_provider_id")
    private UUID priceProviderId;
}
