package com.obsidian.vampireapi.chain;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ChainMapper {

    ChainDTO toDTO(Chain chain);

    Chain fromDTO(ChainDTO chainDTO);
}
