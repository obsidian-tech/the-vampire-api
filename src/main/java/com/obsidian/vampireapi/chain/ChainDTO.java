package com.obsidian.vampireapi.chain;

import com.obsidian.vampireapi.token.TokenDTO;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public final class ChainDTO {

    private Long id;

    private String name;

    private Set<TokenDTO> tokens;
}
