package com.obsidian.vampireapi.chain;

import com.obsidian.vampireapi.core.BaseEntity;
import com.obsidian.vampireapi.token.Token;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Entity
public final class Chain extends BaseEntity {

    @Id
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "chain", fetch = FetchType.EAGER)
    private Set<Token> tokens = new LinkedHashSet<>();
}
